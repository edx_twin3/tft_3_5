/* ************************************************************************** */

#include "tft.h"
#include "dma.h"
/** Descriptive File Name

  @Company
    edx-TwIn3

  @File Name
    tft.c

  @Summary
    Libreria para pantalla 3.5 tft.

  @Description
    Controlador ILI9486.
 */
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

// uint8_t _rotation = 0;
// uint16_t _width = WIDTH, _height = HEIGHT;

static inline void PMP_Write(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin, uint8_t buff){
	GPIOx->BSRR = (buff != GPIO_PIN_SET) ? (uint32_t)GPIO_Pin << 16U : GPIO_Pin;
}

static inline void DMA_Send(DMA_HandleTypeDef *hdma, uint32_t aux, GPIO_TypeDef *GPIOx){
	HAL_DMA_Start_IT(hdma, (uint32_t)&aux, (uint32_t)&(GPIOx->ODR), 1);
}

static inline void PMP_PORT_Write(uint8_t data){
uint32_t aux, tmp;
	CS_ON();
	WR_ON();
//	aux = (data & 0x84);
//	tmp =( (aux & 0x0F)<<4 | (aux & 0xF0)>>4 ); //swaping
//	aux = (aux + tmp) << 7; //8

	aux = data & 0xF4;
	aux =( (aux & 0x0F)<<4 | (aux & 0xF0)>>4 ); //swaping
	tmp = (aux << 1) & 0x10;
	aux &= 0x40;
	aux |= tmp;
	aux = aux << 8;


	#if PMP_DMA
		DMA_Send(&HANDLER_DMA3, aux, GPIOG);
	#else
		GPIOG->ODR = aux;
	#endif
//	aux = (data & 0x78) << 1;
//	tmp =( (aux & 0x0F)<<4 | (aux & 0xF0)>>4 ); //swaping
//	aux = (aux + tmp);
//	tmp =  tmp >> 3;
//	aux = aux & 0x3E;
//	aux = (aux + tmp) << 8; //9

		  aux = (data >> 2) & 0x06;
		  tmp = (data >> 1) & 0x30;
		  aux |= tmp;
		  tmp = tmp >> 1;
		  tmp =( (tmp & 0x0F)<<4 | (tmp & 0xF0)>>4 ); //swaping
		  aux &= 0xA6;
		  aux |= tmp;
		  aux = ( (aux & 0x0F)<<4 | (aux & 0xF0)>>4 );
		  aux = aux << 8;

	#if PMP_DMA
		DMA_Send(&HANDLER_DMA2, aux, GPIOE);
	#else
		GPIOE->ODR = aux;
	#endif
	aux = data << 14; //15
	#if PMP_DMA
		DMA_Send(&HANDLER_DMA1, aux, GPIOD);
	#else
		GPIOD->ODR = aux;
	#endif
	aux = data << 3; //4
#if PMP_DMA
	DMA_Send(&HANDLER_DMA0, aux, GPIOF);
#else
		GPIOF->ODR = aux;
#endif
	WR_OFF();
	CS_OFF();
}

static inline void PMP2PORT(pmp_data *buff){
	WR_ON();
	PMP_Write(LCD_D7_GPIO_Port, LCD_D7_Pin, buff->d7);
	PMP_Write(LCD_D6_GPIO_Port, LCD_D6_Pin, buff->d6);
	PMP_Write(LCD_D5_GPIO_Port, LCD_D5_Pin, buff->d5);
	PMP_Write(LCD_D4_GPIO_Port, LCD_D4_Pin,	buff->d4);
	PMP_Write(LCD_D3_GPIO_Port, LCD_D3_Pin, buff->d3);
	PMP_Write(LCD_D2_GPIO_Port, LCD_D2_Pin, buff->d2);
	PMP_Write(LCD_D1_GPIO_Port, LCD_D1_Pin, buff->d1);
	PMP_Write(LCD_D0_GPIO_Port, LCD_D0_Pin, buff->d0);
	WR_OFF();
}

static inline void DRV_PMP0_Write(uint8_t data){
	pmp_data buff;
	CS_ON();
	memcpy(&buff,&data, sizeof(uint8_t));
	PMP2PORT(&buff);
	CS_OFF();
}

static void tft_cmd(uint8_t data){
    RS_CMD();
    DRV_PMP0_Write(data); }
static void tft_data(uint8_t data){
    RS_DATA();
    DRV_PMP0_Write(data); }

static void reset(void) {
    RST_STATE(RESET_OFF);
    HAL_Delay(20);
    RST_STATE(RESET_ON);
    HAL_Delay(20);
    RST_STATE(RESET_OFF);
    HAL_Delay(100);
}
static void writeRegister8(uint8_t cmd, uint8_t data){
    tft_cmd(cmd);
    tft_data(data); }

#if TEST_TFT
static void flood(uint16_t color, uint32_t len) {
	 tft_cmd(0x2C);	// Memory Write
	        HAL_Delay(1);

	      	  for(int i=0;i<(480);i++)
	      	  {
	      		  for(int j=0;j<320;j++)
	      		  {

	      				  tft_data(0xF8);	//Red Color
	      				  tft_data(0x00);

	      		  }
	      	  }
}
#endif
extern void tft_init(void){
   reset();
// Interface Mode Control
    writeRegister8(0xB0, 0x00);
    tft_cmd(0x11); // Exit Sleep (0x11)
    HAL_Delay(10);
    // Interface Pixel Format, 16 bits / pixel
    writeRegister8(0x3A, 0x55);
    writeRegister8(ILI9486_MADCTL, ILI9486_MAD_DATA_RIGHT_THEN_DOWN);
    writeRegister8(ILI9486_PWCTR3, 0x44);
    tft_cmd(ILI9486_VMCTR1);
    tft_data(0x00);
    tft_data(0x00);
    tft_data(0x00);
    tft_data(0x00);
    // PGAMCTRL(Positive Gamma Control)
    tft_cmd(0xE0);
    tft_data(0x0F);
    tft_data(0x1F);
    tft_data(0x1C);
    tft_data(0x0C);
    tft_data(0x0F);
    tft_data(0x08);
    tft_data(0x48);
    tft_data(0x98);
    tft_data(0x37);
    tft_data(0x0A);
    tft_data(0x13);
    tft_data(0x04);
    tft_data(0x11);
    tft_data(0x0D);
    tft_data(0x00);
    // NGAMCTRL (Negative Gamma Correction)
    tft_cmd(0xE1);
    tft_data(0x0F);
    tft_data(0x32);
    tft_data(0x2E);
    tft_data(0x0B);
    tft_data(0x0D);
    tft_data(0x05);
    tft_data(0x47);
    tft_data(0x75);
    tft_data(0x37);
    tft_data(0x06);
    tft_data(0x10);
    tft_data(0x03);
    tft_data(0x24);
    tft_data(0x20);
    tft_data(0x00);
    // Digital Gamma Control 1
    tft_cmd(0xE2);
    tft_data(0x0F);
    tft_data(0x32);
    tft_data(0x2E);
    tft_data(0x0B);
    tft_data(0x0D);
    tft_data(0x05);
    tft_data(0x47);
    tft_data(0x75);
    tft_data(0x37);
    tft_data(0x06);
    tft_data(0x10);
    tft_data(0x03);
    tft_data(0x24);
    tft_data(0x20);
    tft_data(0x00);
    tft_cmd(0x13); //Normal display on (0x13)
    tft_cmd(0x20);    // Display inversion off (0x20))
    // # Sleep OUT
    tft_cmd(0x11);
    HAL_Delay(200);
    // Display ON
    tft_cmd(0x29);
#if TEST_TFT
    /*FILLSCREEN*/
    flood(RED, HEIGHT*WIDTH);
    while(1);
#endif
}
#if PMP_DMA
static inline void Start_lptim(LPTIM_HandleTypeDef *hlptim, uint32_t Period){
	  /* Enable the Peripheral */
	  __HAL_LPTIM_ENABLE(hlptim);
	  /* Clear flag */
	  __HAL_LPTIM_CLEAR_FLAG(hlptim, LPTIM_FLAG_ARROK);
	  /* Load the period value in the autoreload register */
	  __HAL_LPTIM_AUTORELOAD_SET(hlptim, Period);
	  /* Wait for the completion of the write operation to the LPTIM_ARR register */
	  while ((!(__HAL_LPTIM_GET_FLAG((hlptim), (LPTIM_FLAG_ARROK)))));
	  /* Disable the Peripheral */
	  __HAL_LPTIM_DISABLE(hlptim);
	  /* Enable Autoreload write complete interrupt */
	  __HAL_LPTIM_ENABLE_IT(hlptim, LPTIM_IT_ARROK);
	  /* Enable Autoreload match interrupt */
	  __HAL_LPTIM_ENABLE_IT(hlptim, LPTIM_IT_ARRM);
	  /* Enable the Peripheral */
	  __HAL_LPTIM_ENABLE(hlptim);
	  /* Start timer in continuous mode */
	  __HAL_LPTIM_START_CONTINUOUS(hlptim);
}

static inline void Stop_lptim(LPTIM_HandleTypeDef *hlptim){
	  /* Disable the Peripheral */
	  __HAL_LPTIM_DISABLE(hlptim);
	  /* Disable Autoreload write complete interrupt */
	  __HAL_LPTIM_DISABLE_IT(hlptim, LPTIM_IT_ARROK);
	  /* Disable Autoreload match interrupt */
	  __HAL_LPTIM_DISABLE_IT(hlptim, LPTIM_IT_ARRM);
}
#endif
extern inline void tft_imagen(uint8_t *imagen){
    uint32_t c;
	RD_OFF();
	RS_CMD();
#if PMP_DMA
	Start_lptim(&HANDLER_LPTIM, 1);
#endif
	PMP_PORT_Write(0x2C);
	//DRV_PMP0_Write(0x2C); //Memory Write
    RS_DATA();
    for(c=0;c!=IMA_BUFF_SIZE;c++){
    //PMP_PORT_Write(((uint8_t *)imagen)[c]);
    DRV_PMP0_Write(((uint8_t *)imagen)[c]);
    }
#if PMP_DMA
    Stop_lptim(&HANDLER_LPTIM);
#endif
}   
/* *****************************************************************************
 End of File
 */
