/* ************************************************************************** */
/** Descriptive File Name

  @Company
    edx-TwIn3

  @File Name
    tft.h

  @Summary
    Libreria para pantalla 2.4 tft.

  @Description
    Controlador ILI9341.
 */
/* ************************************************************************** */
#ifndef _TFT_H    /* Guard against multiple inclusion */
#define _TFT_H
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */
#include "main.h"
#include "lptim.h"
#include <stdbool.h>
#include <string.h>
/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif
    /* ************************************************************************** */
    /* ************************************************************************** */
    /* Section: Constants                                                         */
    /* ************************************************************************** */
    /* ************************************************************************** */
#define TEST_TFT            (false)
#define GIF					(true)

#define PMP_DMA             (false)


#define IMA_BUFF_SIZE       (2*(WIDTH*HEIGHT))
#define FRAME_NUM			(86) //38 potato 24 hello 86
//Data or CMD selection, low level: command, high level: data
#define RS_PIN              (LCD_RS_Pin)
#define RS_CMD()              (GPIOC->BSRR = (uint32_t)RS_PIN << 16U)
#define RS_DATA()             (GPIOC->BSRR = (RS_PIN))
//Chip Reset signal active LOW
#define RST_PIN              (LCD_RST_Pin)
#define RESET_ON             (GPIO_PIN_RESET)
#define RESET_OFF            (GPIO_PIN_SET)
#define RST_STATE(x)         (HAL_GPIO_WritePin(LCD_RST_GPIO_Port, RST_PIN, x))
//Chip Select active on LOW
#define CS_PIN                (LCD_CS_Pin)
#define CS_ON()               (GPIOB->BSRR = (uint32_t)LCD_CS_Pin << 16U)
#define CS_OFF()              (GPIOB->BSRR = (CS_PIN))

//Write Data
#define WR_PIN				  (LCD_WR_Pin)
#define WR_ON()				  (GPIOC->BSRR = (uint32_t)WR_PIN << 16U)
#define WR_OFF()			  (GPIOC->BSRR = (WR_PIN))
//Read Data
#define RD_PIN				  (LCD_RD_Pin)
#define RD_ON()				  (GPIOA->BSRR = (uint32_t)LCD_RD_Pin << 16U)
#define RD_OFF()			  (GPIOA->BSRR = (RD_PIN))


#define WIDTH   (320)
#define HEIGHT  (480)




#define ILI9486_NOP            0x00
#define ILI9486_SWRESET        0x01

#define ILI9486_RDDID          0x04
#define ILI9486_RDDST          0x09
#define ILI9486_RDMODE         0x0A
#define ILI9486_RDMADCTL       0x0B
#define ILI9486_RDPIXFMT       0x0C
#define ILI9486_RDIMGFMT       0x0D
#define ILI9486_RDSELFDIAG     0x0F

#define ILI9486_SLPIN          0x10
#define ILI9486_SLPOUT         0x11
#define ILI9486_PTLON          0x12
#define ILI9486_NORON          0x13

#define ILI9486_INVOFF         0x20
#define ILI9486_INVON          0x21
#define ILI9486_GAMMASET       0x26
#define ILI9486_DISPOFF        0x28
#define ILI9486_DISPON         0x29

#define ILI9486_CASET          0x2A
#define ILI9486_PASET          0x2B
#define ILI9486_RAMWR          0x2C
#define ILI9486_RAMRD          0x2E

#define ILI9486_PTLAR          0x30
#define ILI9486_VSCRDEF        0x33
#define ILI9486_MADCTL         0x36
#define ILI9486_VSCRSADD       0x37     /* Vertical Scrolling Start Address */
#define ILI9486_PIXFMT         0x3A     /* COLMOD: Pixel Format Set */

#define ILI9486_RGB_INTERFACE  0xB0     /* RGB Interface Signal Control */
#define ILI9486_FRMCTR1        0xB1
#define ILI9486_FRMCTR2        0xB2
#define ILI9486_FRMCTR3        0xB3
#define ILI9486_INVCTR         0xB4
#define ILI9486_DFUNCTR        0xB6     /* Display Function Control */

#define ILI9486_PWCTR1         0xC0
#define ILI9486_PWCTR2         0xC1
#define ILI9486_PWCTR3         0xC2
#define ILI9486_PWCTR4         0xC3
#define ILI9486_PWCTR5         0xC4
#define ILI9486_VMCTR1         0xC5
#define ILI9486_VMCTR2         0xC7

#define ILI9486_RDID1          0xDA
#define ILI9486_RDID2          0xDB
#define ILI9486_RDID3          0xDC
#define ILI9486_RDID4          0xDD

#define ILI9486_GMCTRP1        0xE0
#define ILI9486_GMCTRN1        0xE1
#define ILI9486_DGCTR1         0xE2
#define ILI9486_DGCTR2         0xE3

//-----------------------------------------------------------------------------
#define ILI9486_MAD_RGB        0x08
#define ILI9486_MAD_BGR        0x00

#define ILI9486_MAD_VERTICAL   0x20
#define ILI9486_MAD_X_LEFT     0x00
#define ILI9486_MAD_X_RIGHT    0x40
#define ILI9486_MAD_Y_UP       0x80
#define ILI9486_MAD_Y_DOWN     0x00

#if ILI9486_COLORMODE == 0
#define ILI9486_MAD_COLORMODE  ILI9486_MAD_RGB
#else
#define ILI9486_MAD_COLORMODE  ILI9486_MAD_BGR
#endif

#if (ILI9486_ORIENTATION == 0)
#define ILI9486_SIZE_X                     ILI9486_LCD_PIXEL_WIDTH
#define ILI9486_SIZE_Y                     ILI9486_LCD_PIXEL_HEIGHT
#define ILI9486_MAD_DATA_RIGHT_THEN_UP     ILI9486_MAD_COLORMODE | ILI9486_MAD_X_RIGHT | ILI9486_MAD_Y_UP
#define ILI9486_MAD_DATA_RIGHT_THEN_DOWN   ILI9486_MAD_COLORMODE | ILI9486_MAD_X_RIGHT | ILI9486_MAD_Y_DOWN
#define ILI9486_MAD_DATA_RGBMODE           ILI9486_MAD_COLORMODE | ILI9486_MAD_X_LEFT  | ILI9486_MAD_Y_DOWN
#elif (ILI9486_ORIENTATION == 1)
#define ILI9486_SIZE_X                     ILI9486_LCD_PIXEL_HEIGHT
#define ILI9486_SIZE_Y                     ILI9486_LCD_PIXEL_WIDTH
#define ILI9486_MAD_DATA_RIGHT_THEN_UP     ILI9486_MAD_COLORMODE | ILI9486_MAD_X_RIGHT | ILI9486_MAD_Y_DOWN | ILI9486_MAD_VERTICAL
#define ILI9486_MAD_DATA_RIGHT_THEN_DOWN   ILI9486_MAD_COLORMODE | ILI9486_MAD_X_LEFT  | ILI9486_MAD_Y_DOWN | ILI9486_MAD_VERTICAL
#define ILI9486_MAD_DATA_RGBMODE           ILI9486_MAD_COLORMODE | ILI9486_MAD_X_RIGHT | ILI9486_MAD_Y_DOWN
#elif (ILI9486_ORIENTATION == 2)
#define ILI9486_SIZE_X                     ILI9486_LCD_PIXEL_WIDTH
#define ILI9486_SIZE_Y                     ILI9486_LCD_PIXEL_HEIGHT
#define ILI9486_MAD_DATA_RIGHT_THEN_UP     ILI9486_MAD_COLORMODE | ILI9486_MAD_X_LEFT  | ILI9486_MAD_Y_DOWN
#define ILI9486_MAD_DATA_RIGHT_THEN_DOWN   ILI9486_MAD_COLORMODE | ILI9486_MAD_X_LEFT  | ILI9486_MAD_Y_UP
#define ILI9486_MAD_DATA_RGBMODE           ILI9486_MAD_COLORMODE | ILI9486_MAD_X_RIGHT | ILI9486_MAD_Y_UP
#elif (ILI9486_ORIENTATION == 3)
#define ILI9486_SIZE_X                     ILI9486_LCD_PIXEL_HEIGHT
#define ILI9486_SIZE_Y                     ILI9486_LCD_PIXEL_WIDTH
#define ILI9486_MAD_DATA_RIGHT_THEN_UP     ILI9486_MAD_COLORMODE | ILI9486_MAD_X_LEFT  | ILI9486_MAD_Y_UP   | ILI9486_MAD_VERTICAL
#define ILI9486_MAD_DATA_RIGHT_THEN_DOWN   ILI9486_MAD_COLORMODE | ILI9486_MAD_X_RIGHT | ILI9486_MAD_Y_UP   | ILI9486_MAD_VERTICAL
#define ILI9486_MAD_DATA_RGBMODE           ILI9486_MAD_COLORMODE | ILI9486_MAD_X_LEFT  | ILI9486_MAD_Y_UP
#endif

#define ILI9486_SETCURSOR(x, y)            {LCD_IO_WriteCmd8(ILI9486_CASET); LCD_IO_WriteData16_to_2x8(x); LCD_IO_WriteData16_to_2x8(x); \
                                            LCD_IO_WriteCmd8(ILI9486_PASET); LCD_IO_WriteData16_to_2x8(y); LCD_IO_WriteData16_to_2x8(y); }

//-----------------------------------------------------------------------------
#define ILI9486_LCD_INITIALIZED    0x01
#define ILI9486_IO_INITIALIZED     0x02



// utils
#define SWAP_INT16(a, b)               { int16_t t = a; a = b; b = t; }
// most used commands
#define _DISPLAY_INVOFF	    0x20
#define _DISPLAY_INVON	    0x21
#define _DISPLAY_COLADDSET  0x2A
#define _DISPLAY_PAGEADDSET 0x2B
#define _DISPLAY_RAMWR	    0x2C
#define _DISPLAY_MADCTL	    0x36
// Color definitions
#define BLACK 0x0000       /*   0,   0,   0 */
#define NAVY 0x000F        /*   0,   0, 128 */
#define DARKGREEN 0x03E0   /*   0, 128,   0 */
#define DARKCYAN 0x03EF    /*   0, 128, 128 */
#define MAROON 0x7800      /* 128,   0,   0 */
#define PURPLE 0x780F      /* 128,   0, 128 */
#define OLIVE 0x7BE0       /* 128, 128,   0 */
#define LIGHTGREY 0xC618   /* 192, 192, 192 */
#define DARKGREY 0x7BEF    /* 128, 128, 128 */
#define BLUE 0x001F        /*   0,   0, 255 */
#define GREEN 0x07E0       /*   0, 255,   0 */
#define CYAN 0x07FF        /*   0, 255, 255 */
#define RED 0xF800         /* 255,   0,   0 */
#define MAGENTA 0xF81F     /* 255,   0, 255 */
#define YELLOW 0xFFE0      /* 255, 255,   0 */
#define WHITE 0xFFFF       /* 255, 255, 255 */
#define ORANGE 0xFD20      /* 255, 165,   0 */
#define GREENYELLOW 0xAFE5 /* 173, 255,  47 */
#define PINK 0xF81F
// touch screen controller
#define Z_THRESHOLD             20
#define TEMPQ_THRESHOLD         20000u
#define XYZQ_THRESHOLD          500u
#define X_RESIST                259.0

 
typedef union{
  struct{
    uint8_t d0:1;
    uint8_t d1:1;
    uint8_t d2:1;
    uint8_t d3:1;
    uint8_t d4:1;
    uint8_t d5:1;
    uint8_t d6:1;
    uint8_t d7:1;
  };
}pmp_data;

void tft_init(void);
void tft_imagen(uint8_t *imagen);
#ifdef __cplusplus
}
#endif

#endif /* _TFT_H */

/* *****************************************************************************
 End of File
 */
