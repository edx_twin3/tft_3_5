/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    dma.c
  * @brief   This file provides code for the configuration
  *          of all the requested memory to memory DMA transfers.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "dma.h"

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/*----------------------------------------------------------------------------*/
/* Configure DMA                                                              */
/*----------------------------------------------------------------------------*/

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */
DMA_HandleTypeDef hdma_dma_generator0;
DMA_HandleTypeDef hdma_dma_generator1;
DMA_HandleTypeDef hdma_dma_generator2;
DMA_HandleTypeDef hdma_dma_generator3;

/**
  * Enable DMA controller clock
  * Configure DMA for memory to memory transfers
  *   hdma_dma_generator0
  *   hdma_dma_generator1
  *   hdma_dma_generator2
  *   hdma_dma_generator3
  */
void MX_DMA_Init(void)
{

  /* Local variables */
  HAL_DMA_MuxRequestGeneratorConfigTypeDef pRequestGeneratorConfig = {0};

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* Configure DMA request hdma_dma_generator0 on DMA1_Stream1 */
  hdma_dma_generator0.Instance = DMA1_Stream1;
  hdma_dma_generator0.Init.Request = DMA_REQUEST_GENERATOR0;
  hdma_dma_generator0.Init.Direction = DMA_MEMORY_TO_PERIPH;
  hdma_dma_generator0.Init.PeriphInc = DMA_PINC_DISABLE;
  hdma_dma_generator0.Init.MemInc = DMA_MINC_DISABLE;
  hdma_dma_generator0.Init.PeriphDataAlignment = DMA_PDATAALIGN_WORD;
  hdma_dma_generator0.Init.MemDataAlignment = DMA_MDATAALIGN_WORD;
  hdma_dma_generator0.Init.Mode = DMA_NORMAL;
  hdma_dma_generator0.Init.Priority = DMA_PRIORITY_VERY_HIGH;
  hdma_dma_generator0.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
  if (HAL_DMA_Init(&hdma_dma_generator0) != HAL_OK)
  {
    Error_Handler();
  }

  /* Configure the DMAMUX request generator for the selected DMA stream */
  pRequestGeneratorConfig.SignalID = HAL_DMAMUX1_REQ_GEN_LPTIM1_OUT;
  pRequestGeneratorConfig.Polarity = HAL_DMAMUX_REQ_GEN_RISING;
  pRequestGeneratorConfig.RequestNumber = 1;
  if (HAL_DMAEx_ConfigMuxRequestGenerator(&hdma_dma_generator0, &pRequestGeneratorConfig) != HAL_OK)
  {
    Error_Handler();
  }

  /* Configure DMA request hdma_dma_generator1 on DMA1_Stream2 */
  hdma_dma_generator1.Instance = DMA1_Stream2;
  hdma_dma_generator1.Init.Request = DMA_REQUEST_GENERATOR1;
  hdma_dma_generator1.Init.Direction = DMA_MEMORY_TO_PERIPH;
  hdma_dma_generator1.Init.PeriphInc = DMA_PINC_DISABLE;
  hdma_dma_generator1.Init.MemInc = DMA_MINC_DISABLE;
  hdma_dma_generator1.Init.PeriphDataAlignment = DMA_PDATAALIGN_WORD;
  hdma_dma_generator1.Init.MemDataAlignment = DMA_MDATAALIGN_WORD;
  hdma_dma_generator1.Init.Mode = DMA_NORMAL;
  hdma_dma_generator1.Init.Priority = DMA_PRIORITY_VERY_HIGH;
  hdma_dma_generator1.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
  if (HAL_DMA_Init(&hdma_dma_generator1) != HAL_OK)
  {
    Error_Handler();
  }

  /* Configure the DMAMUX request generator for the selected DMA stream */
  pRequestGeneratorConfig.SignalID = HAL_DMAMUX1_REQ_GEN_LPTIM1_OUT;
  pRequestGeneratorConfig.Polarity = HAL_DMAMUX_REQ_GEN_RISING;
  pRequestGeneratorConfig.RequestNumber = 1;
  if (HAL_DMAEx_ConfigMuxRequestGenerator(&hdma_dma_generator1, &pRequestGeneratorConfig) != HAL_OK)
  {
    Error_Handler();
  }

  /* Configure DMA request hdma_dma_generator2 on DMA1_Stream3 */
  hdma_dma_generator2.Instance = DMA1_Stream3;
  hdma_dma_generator2.Init.Request = DMA_REQUEST_GENERATOR2;
  hdma_dma_generator2.Init.Direction = DMA_MEMORY_TO_PERIPH;
  hdma_dma_generator2.Init.PeriphInc = DMA_PINC_DISABLE;
  hdma_dma_generator2.Init.MemInc = DMA_MINC_DISABLE;
  hdma_dma_generator2.Init.PeriphDataAlignment = DMA_PDATAALIGN_WORD;
  hdma_dma_generator2.Init.MemDataAlignment = DMA_MDATAALIGN_WORD;
  hdma_dma_generator2.Init.Mode = DMA_NORMAL;
  hdma_dma_generator2.Init.Priority = DMA_PRIORITY_VERY_HIGH;
  hdma_dma_generator2.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
  if (HAL_DMA_Init(&hdma_dma_generator2) != HAL_OK)
  {
    Error_Handler();
  }

  /* Configure the DMAMUX request generator for the selected DMA stream */
  pRequestGeneratorConfig.SignalID = HAL_DMAMUX1_REQ_GEN_LPTIM1_OUT;
  pRequestGeneratorConfig.Polarity = HAL_DMAMUX_REQ_GEN_RISING;
  pRequestGeneratorConfig.RequestNumber = 1;
  if (HAL_DMAEx_ConfigMuxRequestGenerator(&hdma_dma_generator2, &pRequestGeneratorConfig) != HAL_OK)
  {
    Error_Handler();
  }

  /* Configure DMA request hdma_dma_generator3 on DMA1_Stream4 */
  hdma_dma_generator3.Instance = DMA1_Stream4;
  hdma_dma_generator3.Init.Request = DMA_REQUEST_GENERATOR3;
  hdma_dma_generator3.Init.Direction = DMA_MEMORY_TO_PERIPH;
  hdma_dma_generator3.Init.PeriphInc = DMA_PINC_DISABLE;
  hdma_dma_generator3.Init.MemInc = DMA_MINC_DISABLE;
  hdma_dma_generator3.Init.PeriphDataAlignment = DMA_PDATAALIGN_WORD;
  hdma_dma_generator3.Init.MemDataAlignment = DMA_MDATAALIGN_WORD;
  hdma_dma_generator3.Init.Mode = DMA_NORMAL;
  hdma_dma_generator3.Init.Priority = DMA_PRIORITY_VERY_HIGH;
  hdma_dma_generator3.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
  if (HAL_DMA_Init(&hdma_dma_generator3) != HAL_OK)
  {
    Error_Handler();
  }

  /* Configure the DMAMUX request generator for the selected DMA stream */
  pRequestGeneratorConfig.SignalID = HAL_DMAMUX1_REQ_GEN_LPTIM1_OUT;
  pRequestGeneratorConfig.Polarity = HAL_DMAMUX_REQ_GEN_RISING;
  pRequestGeneratorConfig.RequestNumber = 1;
  if (HAL_DMAEx_ConfigMuxRequestGenerator(&hdma_dma_generator3, &pRequestGeneratorConfig) != HAL_OK)
  {
    Error_Handler();
  }

  /* DMA interrupt init */
  /* DMA1_Stream1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream1_IRQn);
  /* DMA1_Stream2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream2_IRQn);
  /* DMA1_Stream3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream3_IRQn);
  /* DMA1_Stream4_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream4_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream4_IRQn);
  /* DMAMUX1_OVR_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMAMUX1_OVR_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMAMUX1_OVR_IRQn);

}

/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

